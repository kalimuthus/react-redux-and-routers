import logo from "./logo.svg";
import "./App.css";

function About() {
  return (
    <div className="Apps">
      <h1>About</h1>
      <p>
        Shopping carts bridge the gap between shopping and buying, so having the
        best shopping cart software is extremely important on your website. It's
        likely that those just starting out in the market may be unfamiliar with
        the concept. Most people, especially those in the ecommerce industry,
        have likely made a purchase online at some point in their lives. That
        said, most consumers don't fully realize the need and capability that
        shopping carts have (besides leading a customer to checkout). A cart
        typically has three common aspects: It stores product information It's a
        gateway for order, catalog and customer management It renders product
        data, categories and site information for user display Another way to
        look at things is as follows: The online shopping cart is similar to the
        tangible ones we use at the supermarket, but it wears many more hats.
        It's also the shelves, the building, the clearance sign, the cash
        register and often the credit card machine relaying information back to
        the bank.
      </p>
      <a href="/shop">Next Page</a>
    </div>
  );
}

export default About;
