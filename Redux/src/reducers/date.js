const date = (state = 16, action) => {
  switch (action.type) {
    case "NextDayDate":
      return state + 1;
    case "YesterDayDate":
      return state - 1;
    default:
      return state;
  }
};

export default date;
