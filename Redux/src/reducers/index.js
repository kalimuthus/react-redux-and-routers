import isLoggedReducer from "./isLogged";
import counterReducer from "./counter";
import dateReducer from "./date";

import { createStore, combineReducers } from "redux";

const store = combineReducers({
  login: isLoggedReducer,
  counter: counterReducer,
  date: dateReducer,
});

export default store;
