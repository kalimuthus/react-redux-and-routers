import logo from "./logo.svg";
import "./App.css";
import { Link } from "react-router-dom";

function Nav() {
  const navStyle = {
    color: "white",
  };
  const colorStyle = {
    color: "red",
  };
  const clrStyle = {
    color: "skyblue",
  };
  return (
    <nav>
      <h3>Welcome</h3>
      <ul className="nav-links">
        <Link style={navStyle} to="/">
          <li>Home</li>
        </Link>
        <Link style={colorStyle} to="/about">
          <li>About</li>
        </Link>
        <Link style={clrStyle} to="/shop">
          <li>Shop</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Nav;
