import logo from "./logo.svg";
import agriculture from "./agriculture.JPG";
import "./App.css";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div className="Apps">
      <h1>Welcome To Home Page </h1>
      <img src={agriculture} className="logo" alt="logo" /> <br />
      <a href="/about">Next Page</a>
    </div>
  );
}

export default Home;
