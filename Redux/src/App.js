import { useSelector, useDispatch } from "react-redux";
import {
  increment,
  decrement,
  isLogged,
  NextDate,
  previousDate,
} from "./actions";
import "./App.css";
import { Apps } from "./Apps";
import Shop from "./Shop";
import Nav from "./Nav";
import About from "./About";
import Home from "./Home";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  const counter = useSelector((state) => state.counter);
  const logged = useSelector((state) => state.login);
  const date = useSelector((state) => state.date);
  const dispatch = useDispatch();

  let signin_status = "";
  if (logged === false) {
    signin_status = "Login";
  } else {
    signin_status = "Logout";
  }

  return (
    <div className="App">
      <h1>Counter : {counter}</h1>
      <h2>Date:{date}</h2>

      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <button onClick={() => dispatch(isLogged())}> {signin_status}</button>
      <button onClick={() => dispatch(NextDate())}>Nxt Date</button>
      <button onClick={() => dispatch(previousDate())}>previousDate</button>

      {logged ? (
        <h1>
          This is a login Page..
          <Router>
            <div className="Apps">
              <Nav />
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/about" component={About} />
                <Route path="/shop" component={Shop} />
              </Switch>
            </div>
          </Router>
        </h1>
      ) : (
        <h1>
          This is Logout Page..!! Please login to see the "Router Functions"
        </h1>
      )}
    </div>
  );
}

export default App;
