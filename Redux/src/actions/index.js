export const increment = () => {
  return {
    type: "INCREMENT",
  };
};
export const decrement = () => {
  return {
    type: "DECREMENT",
  };
};

export const isLogged = () => {
  return {
    type: "SIGN_IN",
  };
};

export const NextDate = () => {
  return {
    type: "NextDayDate",
  };
};
export const previousDate = () => {
  return {
    type: "YesterDayDate",
  };
};
